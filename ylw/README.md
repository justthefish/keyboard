This is a first version of firmware for thefish's YLW custom keyboard.

Based on hasu's [tmk_keyboard](https://github.com/tmk/tmk_keyboard) and Philpirj's [Codename:Hornet](http://habrahabr.ru/post/177347/).

Only basic features are enabled, key layout is far from optimal.
