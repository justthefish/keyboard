/*
Copyright 2012,2013 Jun Wako <wakojun@gmail.com>
Modifications 2013 Philpirj
Modifications 2014 thefish

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdint.h>
#include <stdbool.h>
#include <avr/pgmspace.h>
#include "keycode.h"
#include "action.h"
#include "action_macro.h"
#include "report.h"
#include "host.h"
#include "print.h"
#include "debug.h"
#include "keymap.h"

//Messy macro to represent keyboard layout
#define KEYMAP( \
    K00,K01,K02,K03,K04,K30,            K80,K50,K51,K52,K53,K54, \
    K10,K11,K12,K13,K14,K31,T32,    T81,K82,K60,K61,K62,K63,K64, \
    K20,K21,K22,K23,K24,K33,T34,    T83,K84,K70,K71,K72,K73,K74, \
            T40,T41,T42,T43,T44,    T90,T91,T92,T93,T94 \
        ) { \
    {  KC_##K04, KC_##K03, KC_##K02, KC_##K01, KC_##K00,   KC_##K50, KC_##K51, KC_##K52, KC_##K53, KC_##K54 }, \ 
    {  KC_##K14, KC_##K13, KC_##K12, KC_##K11, KC_##K10,   KC_##K60, KC_##K61, KC_##K62, KC_##K63, KC_##K64 }, \ 
    {  KC_##K24, KC_##K23, KC_##K22, KC_##K21, KC_##K20,   KC_##K70, KC_##K71, KC_##K72, KC_##K73, KC_##K74 }, \
    {  KC_##T44, KC_##T43, KC_##T42, KC_##T41, KC_##T40,   KC_##T90, KC_##T91, KC_##T92, KC_##T93, KC_##T94 }, \ 
    {  KC_##T32, KC_##T34, KC_##K33, KC_##K31, KC_##K30,   KC_##T81, KC_##T83, KC_##K84, KC_##K82, KC_##K80 }  \ 
}

static const uint8_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
 
 KEYMAP( \
    GRV,   Q,   W,   E,   R,   T,                  Y,   U,   I,   O,   P,   EQL, \
   SLSH,   A,   S,   D,   F,   G,   PGUP,  HOME,   H,   J,   K,   L,   SCLN,QUOT, \
   LBRC,   Z,   X,   C,   V,   B,   PGDN,  END,    N,   M,   COMM,DOT, BSLS,RBRC, \
                FN0, FN1, FN2, FN3, FN4,   FN9,    FN8, FN7, FN6, FN5   ),

/** Layer 1: (symbols and navigation)
 *
 *     1 2 3 4 5    6 7 8 9 0 -
 *       ↰ ↑ ↳ ⤒    ← ↓ ↑ →  
 *       ← ↓ → ⤓    
 */
 
  KEYMAP( \
     NO,   1,   2,   3,   4,   5,                    6,   7,   8,   9,   0,MINS, \
     NO,  NO,PGUP,  UP,PGDN,HOME,   NO,     NO,   LEFT,DOWN,  UP,RGHT,SCLN,QUOT, \
     NO,  NO,LEFT,DOWN,RGHT, END,   NO,     NO,   NO,  NO,  NO,  NO,  NO,  NO, \
                FN0, FN1, FN2, FN3, FN4,   FN9,    FN8, FN7, FN6, FN5   ),
/** Layer 2: F-keys **/
  KEYMAP( \
     NO,  F1,  F2,  F3,  F4,  F5,                  F6,  F7,   F8,  F9, F10,MINS, \
     NO,  NO,PGUP,  UP,PGDN,HOME,   NO,     NO,   LEFT,DOWN,  UP,RGHT,SCLN,QUOT, \
     NO,  NO,LEFT,DOWN,RGHT, END,   NO,     NO,   NO,  NO,  NO,  NO,  NO,  NO, \
                FN0, FN1, FN2, FN3, FN4,   FN9,    FN8, FN7, FN6, FN5   )
};

/*
 * Fn action definition
 */
// TODO: use [1] = KEYMAP(...) to prevent from changing index of element?
static const uint16_t PROGMEM fn_actions[] = {
    /* Philpirj setup
    [0] = ACTION_LAYER_TAP_KEY(1, KC_SPC),                 // FN0 = L1 symbols 
    [1] = ACTION_MODS_TAP_KEY(MOD_LSFT, KC_SPC),           // FN1 = Shift with tap Space
    [2] = ACTION_MODS_TAP_KEY(MOD_LCTL, KC_ESC),           // FN2 = Ctrl with tap Escape
    [3] = ACTION_MODS_TAP_KEY(MOD_LALT, KC_TAB),           // FN3 = Alt with tap Tab
    [4] = ACTION_MODS_TAP_KEY(MOD_LGUI, KC_DEL),           // FN4 = Meta with tap Del

    [5] = ACTION_LAYER_TAP_KEY(1, KC_ENT),                 // FN5 = L1 symbols 
    [6] = ACTION_MODS_TAP_KEY(MOD_RSFT, KC_ENT),           // FN6 = Shift with tap Enter
    [7] = ACTION_MODS_TAP_KEY(MOD_RCTL, KC_BSPC),          // FN7 = Ctrl with tap Backspace
    [8] = ACTION_MODS_TAP_KEY(MOD_RALT, KC_RALT),          // FN8 = Ctrl with tap Backspace
    [9] = ACTION_MODS_TAP_KEY(MOD_RGUI, KC_TAB)            // FN9 = Meta with tap Tab
    */
    [0] = ACTION_LAYER_TAP_KEY(1, KC_SPC),                 //FN0 = L1 / space
    [1] = ACTION_MODS_TAP_KEY(MOD_LGUI, KC_TAB),           //FN1 = meta with tap tab
    [2] = ACTION_MODS_TAP_KEY(MOD_LCTL, KC_ESC),           //FN2 = ctrl with tap escape
    [3] = ACTION_MODS_TAP_KEY(MOD_LALT, KC_DEL),           //FN3 = alt with tap del
    [4] = ACTION_MODS_TAP_KEY(MOD_LSFT, KC_TAB),           //FN4 = shift with tap tab

    [9] = ACTION_MODS_TAP_KEY(MOD_RSFT, KC_SPC),           //FN5 = shift with tap space
    [8] = ACTION_MODS_TAP_KEY(MOD_RALT, KC_BSPC),          //FN6 = alt with tap backspace
    [7] = ACTION_MODS_TAP_KEY(MOD_RCTL, KC_ENT),           //FN7 = ctrl with tap enter
    [6] = ACTION_LAYER_TAP_KEY(MOD_RGUI, KC_TAB),          //FN8 = meta with tap tab
    [5] = ACTION_LAYER_TAP_KEY(2, KC_ENT)                  //FN9 = L2 symbols with tap enter
};


#define KEYMAPS_SIZE    (sizeof(keymaps) / sizeof(keymaps[0]))
#define FN_ACTIONS_SIZE (sizeof(fn_actions) / sizeof(fn_actions[0]))

/* translates key to keycode */
uint8_t keymap_key_to_keycode(uint8_t layer, key_t key)
{
    if (layer < KEYMAPS_SIZE) {
        return pgm_read_byte(&keymaps[(layer)][(key.row)][(key.col)]);
    } else {
        // XXX: this may cuaes bootlaoder_jump inconsistent fail.
        //debug("key_to_keycode: base "); debug_dec(layer); debug(" is invalid.\n");
        // fall back to layer 0
        return pgm_read_byte(&keymaps[0][(key.row)][(key.col)]);
    }
}

/* translates Fn keycode to action */
action_t keymap_fn_to_action(uint8_t keycode)
{
    action_t action;
    if (FN_INDEX(keycode) < FN_ACTIONS_SIZE) {
        action.code = pgm_read_word(&fn_actions[FN_INDEX(keycode)]);
    } else {
        action.code = ACTION_NO;
    }
    return action;
}
